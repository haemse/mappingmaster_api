const JSON5 = require('json5');


convert = str => {
    var regiboy = /dwr\.engine\.remote\.handleCallback\(("|')\d{1,4}("|'),("|')\d{1,4}("|'),(.+)\);/gm;

    try{
        let match = regiboy.exec(str);

        if (match[5]) {
            let obj_str = match[5];

            let obj = JSON5.parse(obj_str);

            return Array.isArray(obj) ? obj : new Error("parsed obj from response is not an array - MMResponseToObj.js"); 
        }else{
            return new Error("no expected subgroup match on regex - MMResponseToObj.js");
        }
    } catch (e) {
        e.message = "MMResponseToObj: " + e.message;
        return e;
    }
}

module.exports = convert;



