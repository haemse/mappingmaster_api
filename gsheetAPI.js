/***************************************************************************************************************
 * @Author: jacob.haemmerle 
 * @Date: 2018-06-28 00:11:03 
 * @Last Modified by: jacob.haemmerle
 * @Last Modified time: 2018-06-30 04:22:00
 **************************************************************************************************************/

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

const JSON5 = require('json5')

const ssID = '1Uiy6vWlApEMe3ggxqjyDb-r4uoNH3VufQgQde3SdmPo'

// If modifying these scopes, delete credentials.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = 'credentials.json';

// EXPORTS __________________________________________________________________________________________________________________________________

fillKomfortSuites = (data_obj) =>{
    var prom = new Promise((resolve, reject) => {
        insertKomfort.promise.resolve = resolve;
        insertKomfort.promise.reject = reject;
        // Load client secrets from a local file.
        fs.readFile('client_secret.json', (err, content) => {
            if (err) return console.log('Error loading client secret file:', err);
            // Authorize a client with credentials, then call the Google Sheets API.
            authorize(JSON.parse(content), insertKomfort);
        });
    });

    return prom;
}

fillCatData = (data_obj, req_beacon) =>{
    return new Promise((resolve, reject) => {
        insertCatData.promise.resolve = resolve;
        insertCatData.promise.reject = reject;

        var data = [];

        //prepare data value obj according to sheet batch update api
        req_beacon.cats.forEach(beacon_cat => {

            var vals = data_obj.find(mm_cat => mm_cat.roomCode == beacon_cat.id).roomData.map((day) => {
            return [day.date, day.room_no_of_units, day.minlos, day.price1];
            });

            data.push({
            range: beacon_cat.insertToday,
            majorDimension: 'ROWS',
            values: vals
            });
        
        });

        insertCatData.data = data;
        insertCatData.req_beacon = req_beacon;
        // Load client secrets from a local file.
        fs.readFile('client_secret.json', (err, content) => {
            if (err) return console.log('Error loading client secret file:', err);
            // Authorize a client with credentials, then call the Google Sheets API.
            authorize(JSON.parse(content), insertCatData);
        });
    });
}

getCatData = (ranges_arr) => {
    return new Promise((resolve, reject) => {
        reqCatData.promise.resolve = resolve;
        reqCatData.promise.reject = reject;

        reqCatData.ranges = ranges_arr;

        // Load client secrets from a local file.
        fs.readFile('client_secret.json', (err, content) => {
            if (err) return console.log('Error loading client secret file:', err);
            // Authorize a client with credentials, then call the Google Sheets API.
            authorize(JSON.parse(content), reqCatData);
        });
    });
}

// AUTH & EXEC ______________________________________________________________________________________________________________________________

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback.funct(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return callback.funct(err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback.funct(oAuth2Client);
    });
  });
}

/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function listMajors(auth) {
  const sheets = google.sheets({version: 'v4', auth});
  sheets.spreadsheets.values.get({
    spreadsheetId: '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms',
    range: 'Class Data!A2:E',
  }, (err, {data}) => {
    if (err) return console.log('The API returned an error: ' + err);
    const rows = data.values;
    if (rows.length) {
      console.log('Name, Major:');
      // Print columns A and E, which correspond to indices 0 and 4.
      rows.map((row) => {
        console.log(`${row[0]}, ${row[4]}`);
      });
    } else {
      console.log('No data found.');
    }
  });
}

// CALLBACKS & OBJ __________________________________________________________________________________________________________________________

insertKomfort = {
    args: {test: "test"},
    promise: {},
    callback: {},
    funct: function (auth) {
        var prom = this.promise;

        var sheets = google.sheets('v4');
        var request = {
            auth: auth,
            spreadsheetId: ssID,
            includeValuesInResponse: false,
            resource: {
                // How the input data should be interpreted.
                //https://developers.google.com/sheets/api/reference/rest/v4/ValueInputOption
                valueInputOption: 'USER_ENTERED',  // TODO: Update placeholder value.
            
                // The new values to apply to the spreadsheet.
                //https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values#ValueRange
                data: [
                    {
                        range: 'Tage!G4',
                        majorDimension: 'ROWS',
                        values: [
                            [1],
                            [1],
                            [1],
                            [1],
                            [1],
                            [1],
                            [1],
                            [1],
                            [1],
                            [1]
                        ]
                    }
                ],  // TODO: Update placeholder value.
            
                // TODO: Add desired properties to the request body.
            },
        }

        sheets.spreadsheets.values.batchUpdate(request, function(err, response) {
            if (err) {
                console.log("erriboy");
                console.error(err);
                return;
            }

            response.status && console.log(response.status);

            
            //this.promise.resolve();
            prom.resolve(response.status);
            
            // const cache = new Map();
            // // TODO: Change code below to process the `response` object:
            // console.log(JSON.stringify(response, function (key, value) {
            //     if (typeof value === 'object' && value !== null) {
            //       if (cache.get(value)) {
            //         // Circular reference found, discard key
            //         return;
            //       }
            //       // Store value in our map
            //       cache.set(value, true);
            //     }
            //     return value;
            //   },2));
        });
    }
}

insertCatData = {
    promise: {},
    funct: function (auth) {
        var prom = this.promise;

        var sheets = google.sheets('v4');
        var request = {
            auth: auth,
            spreadsheetId: ssID,
            includeValuesInResponse: false,
            resource: {
                // How the input data should be interpreted.
                //https://developers.google.com/sheets/api/reference/rest/v4/ValueInputOption
                valueInputOption: 'USER_ENTERED',  // TODO: Update placeholder value.
            
                // The new values to apply to the spreadsheet.
                //https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values#ValueRange
                data: this.data,  // TODO: Update placeholder value.
            
                // TODO: Add desired properties to the request body.
            },
        }

        sheets.spreadsheets.values.batchUpdate(request, function(err, response) {
            if (err) {
                console.log("erriboy");
                console.error(err);
                prom.reject(err);
                return;
            }

            response.status && console.log("batchUpdate http response code: " + response.status);

            
            //this.promise.resolve();
            prom.resolve(response.status);
            
        });
    }
}

reqCatData = {
    promise: {},
    ranges: [],
    funct: function (auth) {
        var prom = this.promise;

        var sheets = google.sheets('v4');

        var request = {

            // The ID of the spreadsheet to retrieve data from.
            spreadsheetId: ssID,     //'1zm7UhyQiMbaQCRgO8jtXEwxCjMZUkHwkSAye5vhQR8U', // ssID,  // TODO: Update placeholder value.
        
            // The A1 notation of the values to retrieve.
            ranges: this.ranges,  // TODO: Update placeholder value.
        
            // How values should be represented in the output.
            // The default render option is ValueRenderOption.FORMATTED_VALUE.
            valueRenderOption: 'UNFORMATTED_VALUE',  // TODO: Update placeholder value.
        
            // How dates, times, and durations should be represented in the output.
            // This is ignored if value_render_option is
            // FORMATTED_VALUE.
            // The default dateTime render option is [DateTimeRenderOption.SERIAL_NUMBER].
            dateTimeRenderOption: 'SERIAL_NUMBER',  // TODO: Update placeholder value.
        
            auth: auth,
        };
        
        sheets.spreadsheets.values.batchGet(request, function(err, response) {
            if (err) {
                console.log("erriboy");
                console.error(err);
                prom.reject(err);
                return;
            }

            response.status && console.log("batchGet http response code: " + response.status);

            prom.resolve(response.data);
            
            var cache = [];
            // TODO: Change code below to process the `response` object:
            fs.writeFileSync('./getCatData.json',JSON.stringify(response.data, function(key, value) {
                if (typeof value === 'object' && value !== null) {
                    if (cache.indexOf(value) !== -1) {
                        // Duplicate reference found
                        try {
                            // If this value does not reference a parent it can be deduped
                            return JSON.parse(JSON.stringify(value));
                        } catch (error) {
                            // discard key if value cannot be deduped
                            return;
                        }
                    }
                    // Store value in our collection
                    cache.push(value);
                }
                return value;
            }, 2));
            cache = null;
        });
    }
}



  module.exports = {fillCatData: fillCatData, getCatData: getCatData}
