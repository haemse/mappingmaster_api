var log_store = [];

log = (log) => {
    log_store.push([getDateStr(), 'server', 'log', log]);
    console.log(log);
}

error = (log) => {
    log_store.push([getDateStr(), 'server', 'error', log]);
    console.error(log);
}

warn = (log) => {
    log_store.push([getDateStr(), 'server', 'warn', log]);
    console.error(log);
}

getLogsAndClear = () => {
    let logs = log_store.slice();
    log_store = [];
    return logs
}

clear = (log) => {
    log_store = [];
}

getDateStr = () =>{
    let d = new Date();
    return [
            d.getFullYear(),
            ('0' + (d.getMonth()+1)).slice(-2),
            ('0' + d.getDate()).slice(-2)
        ].join(' ')+' '+
        [
            d.getHours(),
            d.getMinutes(),
            d.getSeconds()
        ].join(':');
}

module.exports = {
    log, warn, error, clear, getLogsAndClear
}