var fs = require('fs');
require("./PrototypeExtender");

var pushObj_days = {
    cat_list:[
        {id: '145296', name: ''},
        {id: '145298', name: ''},
        {id: '145300', name: ''}
    ],

    days: [
        {
            date: '2018-06-07',
            room_cats: {
                '145296' : {
                    price: 11,
                },
                '145298' : {
                    price: 22,
                },
                '145300' : {
                    price: 33,
                }
            }
        },
        {
            date: '2018-06-08',
            room_cats: {
                '145296' : {
                    price: 11,
                },
                '145298' : {
                    price: 22,
                },
                '145300' : {
                    price: 33,
                }
            }
        },
        {
            date: '2018-06-09',
            room_cats: {
                '145296' : {
                    price: 11,
                    secondParam: "test"
                },
                '145298' : {
                    price: 22,
                },
                '145300' : {
                    price: 33,
                }
            }
        },
    ]
};

  var pushObj_days_test85 = {
    cat_list:[
        {id: '145296', name: ''},
        {id: '145298', name: ''},
        {id: '145300', name: ''}
    ],

    days: [
        {
            date: '2018-06-08',
            room_cats: {
                '145296' : {
                    price: 85,
                },
                '145298' : {
                    price: 96,
                },
                '145300' : {
                    price: 141,
                },
            }
        },
        {
            date: '2018-06-09',
            room_cats: {
                '145296' : {
                    price: 85,
                },
                '145298' : {
                    price: 98,
                },
                '145300' : {
                    price: 148,
                },
            }
        }
    ]
};

var pushObj_days_test85_original = {
    cat_list:[
        {id: '145296', name: ''},
        {id: '145298', name: ''},
        {id: '145300', name: ''}
    ],

    days: [
        {
            date: '2018-06-08',
            room_cats: {
                '145296' : {
                    price: 81,
                },
                '145298' : {
                    price: 95,
                },
                '145300' : {
                    price: 140,
                },
            }
        },
        {
            date: '2018-06-09',
            room_cats: {
                '145296' : {
                    price: 66,
                },
                '145298' : {
                    price: 99,
                },
                '145300' : {
                    price: 149,
                },
            }
        }
    ]
  };

categorize = (obj, cat) => obj.reduce((newArr, a) => {if (a.room_cats[cat]){newArr.push({date: a.date, params: a.room_cats[cat]})}return newArr}, [] );

topushObj_cat = (pushObj_days) => {
    var pushObj_cat = {
        cat_list: pushObj_days.cat_list,
        cats_data: []
    }
    pushObj_days.cat_list.map((a) => {
        pushObj_cat.cats_data.push({cat_id: a.id, cat_days: categorize(pushObj_days.days, a.id)});
    });

    return pushObj_cat;
}

//var pushObj_cat = topushObj_cat(pushObj_days_test85_original);

//console.log(JSON.stringify(pushObj_cat, null, 2));

toMMPost = (pushObj_days, sessionID, scriptSessionID) => {

    var cat_obj = topushObj_cat(pushObj_days);

    var bodyString = "";

    var start_tmpl =
    `callCount=1
    windowName=
    c0-scriptName=FourteenDayCalDWR
    c0-methodName=updateFourteenDayCalendar
    c0-id=0`
    .dedent();

    bodyString += start_tmpl;

    var num_of_days = 2;

    var i = 1;
    var cat_i = 1;
    var days_i = 1;

    numOfVarsPerDay = 15;

    var cat_obj_IDs = []

    //footer
    var batchId = 14;
    sessionID = sessionID ? sessionID : '7d9ZhNClOGPfkrxiVjZd+eFD.undefined';
    scriptSessionID = scriptSessionID ? scriptSessionID : 'EAA00F9B0D47F9BA3ABF9937B30EE7BA';

    cat_obj.cats_data.map( (cat) => {

        let cat_begin_i = i;

        const cat_header = '\n' + `c0-e${i+1}=string:${cat.cat_id}`;

        bodyString += cat_header;

        var day_chief_obj_IDs = [];

        cat.cat_days.map((day) => {
            let day_tmpl =
                `c0-e${i+4}=number:-1
                c0-e${i+5}=number:${day.params.price}
                c0-e${i+6}=number:-1
                c0-e${i+7}=number:-1
                c0-e${i+8}=number:-1
                c0-e${i+9}=number:-1
                c0-e${i+10}=string:${day.date}
                c0-e${i+11}=number:-1
                c0-e${i+12}=number:-1
                c0-e${i+13}=number:-1
                c0-e${i+14}=number:-1
                c0-e${i+15}=number:-1
                c0-e${i+16}=number:-1
                c0-e${i+17}=null:null
                c0-e${i+3}=Object_Object:{availability:reference:c0-e${i+4}, price1:reference:c0-e${i+5}, price2:reference:c0-e${i+6}, price3:reference:c0-e${i+7}, PPP:reference:c0-e${i+8}, PPC:reference:c0-e${i+9}, date:reference:c0-e${i+10}, minlos:reference:c0-e${i+11}, maxlos:reference:c0-e${i+12}, CFS:reference:c0-e${i+13}, CTB:reference:c0-e${i+14}, CTA:reference:c0-e${i+15}, CTD:reference:c0-e${i+16}, BG:reference:c0-e${i+17}}`
                .dedent();
            
            day_chief_obj_IDs.push(i+3);

            bodyString += '\n' + day_tmpl;
            i += numOfVarsPerDay;
        });

        const cat_Objs =
            `c0-e${cat_begin_i+2}=array:[` + day_chief_obj_IDs.map(i => 'reference:c0-e' + i).toString() + ']' +
            '\n' +
            `c0-e${cat_begin_i}=Object_Object:{roomcode:reference:c0-e${cat_begin_i+1}, dayLevelDataList:reference:c0-e${cat_begin_i+2}}`
            .dedent();
        
        bodyString += '\n' + cat_Objs;

        i += 3;

        cat_obj_IDs.push(cat_begin_i);

        cat_i++;
    });


    const end_tmpl =
    `c0-param0=array:[` + cat_obj_IDs.map(i => 'reference:c0-e' + i).toString() + ']' + '\n' + //reference:c0-e1,reference:c0-e34,reference:c0-e52]
    `batchId=${batchId}
    page=%2FCultuzzHotelExtranet%2Ffourteendaycal.htm%3Fmn%3D5000
    httpSessionId=${sessionID}
    scriptSessionId=${scriptSessionID}`
    .dedent();

    bodyString += '\n' + end_tmpl;

    fs.writeFileSync('./requ_obj_current.txt', bodyString);

    return bodyString;
}

//var bodyString = toMMPost(pushObj_cat);

// console.log(bodyString);
//fs.writeFileSync('./requ_obj_current.txt', bodyString);





//__________________________________________________________________________________________________________________________

module.exports = {
    toMMPost,
    pushObj_days_test85,
    pushObj_days_test85_original
}

