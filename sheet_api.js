var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];  //.readonly - dismissed to get write permission
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';

TOKEN_DIR = './';
var TOKEN_PATH = TOKEN_DIR + 'sheets.googleapis.com-nodejs-konsumationen-documentation.json';

var user = [];

function updateUser(){
    // Load client secrets from a local file.
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
        console.log('Error loading client secret file: ' + err);
        return;
        }
        // Authorize a client with the loaded credentials, then call the
        // Google Sheets API.
        authorize(JSON.parse(content), storeUsers);
    });
}

function appendLog(arr){
  // Load client secrets from a local file.
  fs.readFile('client_secret.json', function processClientSecrets(err, content) {
      if (err) {
      console.log('Error loading client secret file: ' + err);
      return;
      }
      // Authorize a client with the loaded credentials, then call the
      // Google Sheets API.
      authorize(JSON.parse(content), appendLine, arr);
  });
}


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback, arr) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback, arr);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client, arr);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback, arr) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client, arr);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}

//------------------------------------------------------------------------------------------------------------------------

/**
 * Print the names and majors of students in a sample spreadsheet:
 * https://docs.google.com/spreadsheets/d/1unl1AgRPMqOgoSv2ibCzhSLntN646wx8dJc9cP8AkF8/edit
 */
function storeUsers(auth) {
  var sheets = google.sheets('v4');
  sheets.spreadsheets.values.get({
    auth: auth,
    spreadsheetId: '1tBmHSviLP6oGV2SmI5IQIvOuu2eo0HRKlUop4HsNxzE',
    range: 'Benutzertabelle!A13:G50',
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var rows = response.values;

    user = rows.map(function(x) {
        return {    
            "display_name": x[0],
            "active": x[1],
            "id_hash": x[2],
            "link": x[3],
            "mitarbeiter_essen": x[4],
            "eigenkonsum": x[5],
            "abgelaufene_produkte": x[6]
        }
    })

    //console.log(JSON.stringify(user,null, 2));

    // if (rows.length == 0) {
    //   console.log('No data found.');
    // } else {
    //   console.log('display_name, aktiv, id_hash, link:');
    //   for (var i = 0; i < rows.length; i++) {
    //     var row = rows[i];
    //     // Print columns A and E, which correspond to indices 0 and 4.
    //     console.log('%s, %s, %s, %s', row[0], row[1], row[2], row[3]);
    //   }
    // }
  });
}

function appendLine(auth, arr) {
  var sheets = google.sheets('v4');
  var request = {
    auth: auth,
    spreadsheetId: '1tBmHSviLP6oGV2SmI5IQIvOuu2eo0HRKlUop4HsNxzE',
    range: 'tryAgains',
    // How the input data should be interpreted.
    valueInputOption: 'USER_ENTERED',  // TODO: Update placeholder value.
    // How the input data should be inserted.
    insertDataOption: 'INSERT_ROWS',  // TODO: Update placeholder value.
    resource: {
      values: [arr]
    }
  }
  sheets.spreadsheets.values.append(request, function(err, response) {
    if (err) {
      console.error(err);
      return;
    }

    // TODO: Change code below to process the `response` object:
    //console.log(JSON.stringify(response, null, 2));
  });
}

function getUser(){
  return user;
}

module.exports = {getUser, updateUser, appendLog};  