var str = "['test': {'text' : 'First Option',  'value' : 'first'},{'text': 'Second Option', 'value': 'second'},{'text': 'Third Option',  'value': 'third'}]";

var str1 = `{
    "2018-06-10": {
      "checkFirstOrLastOfMonth": 0,
      "day": "10",
      "dayOfWeek": "SONNTAG",
      "formatedDate": "10 Jun 2018",
      "monthNo": "06",
      "shortMonthName": "Jun",
      "weekOfYear": 24,
      "year": "2018"
    },
    "2018-06-11": {
      "checkFirstOrLastOfMonth": 0,
      "day": "11",
      "dayOfWeek": "MONTAG",
      "formatedDate": "11 Jun 2018",
      "monthNo": "06",
      "shortMonthName": "Jun",
      "weekOfYear": 24,
      "year": "2018"
    }
}`;

console.log(JSON.stringify(JSON.parse(str1), null, 2));