/***************************************************************************************************************
 * @Author: jacob.haemmerle 
 * @E-Mail: jacob@comcuro.com
 * @Date: 2018-06-14 15:48:47 
 * @Last Modified by: jacob.haemmerle
 * @Last Modified time: 2018-07-01 23:27:42
 * Desc: on HTTP Req, script pulls data from MappingMaster Channel Manager and fills it in a google sheet
 * via google api and the other way around (gsheet -> MM)
 * also it acts as a server to send future prices for set categories - to req and use by the lodgit client an the AH_Managment PC
 * triangle (Lodgit <- MappingmasterAPI <-> gSheet File) <-> Mappingmaster
 **************************************************************************************************************/

const MM = require('./MappingMaster');
const MMconv = require('./convertMMBody_14Days.js');
const gsheet = require('./gsheetAPI');
const porcGASResp = require('./GASResponseProc');
const lggr = require('./log');
const bodyParser = require('body-parser')
const fs = require('fs');

var express = require('express');
var app = express();

var days = 60;
var from = new Date();
var to = new Date();

to.setDate(from.getDate() + days);

from = toMMString(from);
to = toMMString(to);

const test_range = {
    from: from,
    to: to
}

var NEW_PRICES_4Lodgit = false;
//MM.getZimmerRange({room_cats: room_cats, range: test_range});

//gsheet.fillKomfortSuites().then(val => lggr.log("main: " + val));

app.use(bodyParser.json({
    limit: '80mb'
}));

// gsheet.getCatData(['Date', 'KS_Price_Result', 'LS_Price_Result', 'LS_2SZ_Price_Result']).then((resp) => {

//     var myObj = porcGASResp.aggregate(resp);
//     fs.writeFileSync('./aggrGASObj.json', JSON.stringify(myObj, null, 2));
//     myObj = porcGASResp.stripPast(myObj);

//     fs.writeFileSync('./strippedGASObj.json', JSON.stringify(myObj, null, 2));
// });



app.get('/areThereNewPrices/:x', function (req, res) {
    if (req.params.x = "CCD4A124C00886D0902677BA40D13D1D4E7D68676238152A19223E8953E75F9E"){
            res.status(200).send(NEW_PRICES_4Lodgit);
    }
});

app.get('/giveMeThePrices/:x', function (req, res) {
    if (req.params.x = "CCD4A124C00886D0902677BA40D13D1D4E7D68676238152A19223E8953E75F9E"){

        gsheet.getCatData(['Date', 'KS_Price_Result', 'LS_Price_Result', 'LS_2SZ_Price_Result']).then((resp) => {

            var myObj = porcGASResp.aggregate(resp);
            fs.writeFileSync('./aggrGASObj.json', JSON.stringify(myObj, null, 2));
            myObj = porcGASResp.stripPast(myObj);
        
            fs.writeFileSync('./strippedGASObj.json', JSON.stringify(myObj, null, 2));

            NEW_PRICES_4Lodgit = false;

            res.status(200).send(myObj);
        });
    }
});

app.post('/:x/updateSheet', function (req, res) {
    //Template to use for debugging instead of the client ones
    const MM_req_beacon_tmpl = {
        cats: [
            {id: '145296', name: 'Komfort Suite', insertToday: 'Tage!G165'},
            {id: '145298', name: 'Luxus Suite', insertToday: 'Tage!O165'},
            {id: '145300', name: 'Luxus Suite mit 2 Schlafzimmer', insertToday: 'Tage!W165'}
        ],
        sheetID: '1Uiy6vWlApEMe3ggxqjyDb-r4uoNH3VufQgQde3SdmPo',
        range: {
            from: from,
            to: to
        }
    }

    //check link param hash
    if (req.params.x = "52DFC3A472E75BD02C430CB03208D2D5BD6C206EEBF7D75DC7CCEE50AB9EFE8C"){

        lggr.log("incoming sheet price update request");

        var MM_req_beacon = req.body;

        if(MM_req_beacon.sheetID){
            lggr.log("sheedID: " + MM_req_beacon.sheetID);
        }else{
            lggr.log("no sheedID received in request bacon");
            return false;
        }

        MM.login()
        .then((val) => {
            lggr.log(val);
            return MM.getZimmerRange({room_cats: MM_req_beacon.cats, range: MM_req_beacon.range, saveResponse: './zimmer_range_response_1'});
        })
        .then((ZRObj) => {
            lggr.log("MM.getZimmerRange resolved");

            if (ZRObj[2] == null){
                lggr.log("no room objs in response array - ZRObj[2] == null");
                lggr.log("Requesting a second Time. This time there might be a new scriptSessionID from last req.");
                return MM.getZimmerRange({room_cats: MM_req_beacon.cats, range: MM_req_beacon.range, saveResponse: './zimmer_range_response_1'});
            }else{
                lggr.log("room objs RECEIVED");
                return Promise.resolve(ZRObj);
            }
        })
        .then((ZRObj) => {
            if (ZRObj[2] != null){
                //lggr.log('in THEN:',"\n" + JSON.stringify(ZRObj, null, 2).split('\n').slice(0,25).join('\n') + "\n...\n");
                //return MM.updateRange(MMconv.pushObj_days_test85_original);
                //lggr.log(JSON.stringify(ZRObj, null, 2));
                //gsheet.fillKomfortSuites()
                //Use Google API to fill the Data to the sheet
                return gsheet.fillCatData(ZRObj[2], MM_req_beacon);
            }
        })
        .then((val)=>{
            lggr.log("Return: " + val);
            return MM.logout();
        })
        .then((val)=>{
            lggr.log("Return: " + val.red);
            let logArray = lggr.getLogsAndClear();
            res.status(200).send(logArray);
        })
        .catch((error) => {
            MM.logout().then((val) => {
                lggr.log(val);
            });
            lggr.error("Promise Chain CATCH: " + error.message); lggr.error("Promise Chain CATCH: " + "something went wrong");
            let logArray = lggr.getLogsAndClear();
            res.status(500).send(logArray);
        });
    }
});

app.post('/:x/updateMM', function (req, res) {
    if (req.params.x = "FEED141B675BFC6B0D2F1E6FC78397A40FE19095637F9A0629982D8061D12167"){
        lggr.log("incoming MM price update request");
        let req_obj = req.body;

        fs.writeFileSync('reqObj_GAS.json', JSON.stringify(req_obj, null, 2));

        //console.log(JSON.stringify(req_obj, null, 2));

        MM.login()
        .then((val) => {
            lggr.log("Login resolved");
            lggr.log("Return: " + val);
            return MM.updateRange(req_obj);
        })
        .then((val) => {
            lggr.log("MM.updateRange resolved");
            lggr.log("Return: " + val);
            return MM.logout();
        }).then((val) => {
            lggr.log("MM.logout resolved");
            lggr.log("Return: " + val);

            NEW_PRICES_4Lodgit = true;
            
            let logArray = lggr.getLogsAndClear();
            res.status(200).send(logArray);
        })
        .catch((error) => {
            MM.logout().then((val) => {
                lggr.log("Return: " + val);
            });
            lggr.error("Promise Chain CATCH: " + error.message);
            let logArray = lggr.getLogsAndClear();
            res.status(500).send(logArray);
        });
    }
});

app.post('/:x/sendObj', function (req, res) {
    console.log("da kommt was rein - sendObj");
    if (req.params.x = "FEED141B675BFC6B0D2F1E6FC78397A40FE19095637F9A0629982D8061D12167"){

        res.status(200).send('match');
    }
});

login = () => {
    MM.login()
    .then(
        (val) => {
            lggr.log(val);
            return MM.getZimmerRange({room_cats: room_cats, range: test_range});
        })
    .then(
        (ZRObj) => {
            lggr.log("MM.getZimmerRange resolved");
            //lggr.log('in THEN:',"\n" + JSON.stringify(ZRObj, null, 2).split('\n').slice(0,25).join('\n') + "\n...\n");
            //return MM.updateRange(MMconv.pushObj_days_test85_original);
            //gsheet.fillKomfortSuites()
            if (ZRObj[2] == null){
                lggr.log("no room objs in response array - ZRObj[2] == null");
                return MM.getZimmerRange({room_cats: room_cats, range: test_range});
            }else{
                lggr.log("room objs RECEIVED");
                return Promise.resolve(ZRObj);
            }
        })
    .then(
        (ZRObj) => {
            //lggr.log('in THEN:',"\n" + JSON.stringify(ZRObj, null, 2).split('\n').slice(0,25).join('\n') + "\n...\n");
            //return MM.updateRange(MMconv.pushObj_days_test85_original);
            lggr.log(JSON.stringify(ZRObj, null, 2));
            //gsheet.fillKomfortSuites()
        })
    .then((val)=>{
            return MM.logout();
        })
    .then((val)=>{
            lggr.log(val.red);
        })
    .catch((error) => {
            MM.logout().then((val) => {
                lggr.log(val);
            });
            lggr.error("Promise Chain CATCH: " + error.message); lggr.error("Promise Chain CATCH: " + "something went wrong");
        }
    );
}

app.listen(8191, function () {
    lggr.log('Trigger Server listening on port 8191  ');
    });


function toMMString(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();
    
    var mmChars = mm.split('');
    var ddChars = dd.split('');
    
    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
}