/***************************************************************************************************************
 * @Author: jacob.haemmerle 
 * @Date: 2018-06-28 00:08:56 
 * @Last Modified by: jacob.haemmerle
 * @Last Modified time: 2018-06-28 02:15:50
 * Info: Slices off past and returns array [[dates],[Cat1],[Cat2],[Cat3]]
 **************************************************************************************************************/

 const moment = require('moment-timezone');

aggregateToIntoOneArray = (resp) => {
    return resp.valueRanges.map((a, i) => {
        return a.values.map((b) => {
            return i===0 ? SerialNumberDateToJSDate(b[0]) : (typeof b[0] === 'number' ? (Math.round(b[0] * 100) / 100) : b[0]) ;
        });
    });

}

stripPast = (obj) => {
    var now2 = moment.tz('Europe/Vienna').startOf('day');

    today = new Date(Date.UTC(now2.year(), now2.month(),now2.date()));

    // console.log(Number(obj[0][22]));
    // console.log(+today);
    // console.log(now2.valueOf());
    

    var i = obj[0].map(Number).indexOf(+today);

    //console.log(JSON.stringify({date: today}));

    console.log('stripping index i=', i);
    //console.log(today);

    //console.log("obj[0][21]:", obj[0][21]);

    return obj.map((a) => {
        return a.slice(i, a.length);
    });
}

function SerialNumberDateToJSDate(date) {
    var d = new Date(Math.round((date - 25569)*86400*1000));
    return d;
}

module.exports= {aggregate: aggregateToIntoOneArray, stripPast: stripPast}