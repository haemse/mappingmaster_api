/***************************************************************************************************************
 * @Author: jacob.haemmerle 
 * @Date: 2018-06-15 00:25:56 
 * @E-Mail: jacob@comcuro.com
 * @Last Modified by: jacob.haemmerle
 * @Last Modified time: 2018-06-17 22:33:41
 **************************************************************************************************************/

var qs = require("querystring");
var http = require("https");
var request = require('request');
var MMResponseToObj = require("./MMResponseToObj");
const fs = require('fs');
const MMconv = require('./convertMMBody_14Days');
const lggr = require('./log');
require("./PrototypeExtender");

var colors = require('colors');
var beautify = require('js-beautify').js_beautify;


var cookie = [];
var scriptSessionId = 'A4004204FD0DD1AC488D39542355EBD0';

const price_val = 50;
const postmanSession = false;

const staticSessionID = 'KtUvu+2wRRQkqv95fUTNDjhf.undefined';
const staticHostIP = '84.115.217.171';

const loginBodyObj = {
    bName: 'chrome',
    bVersion: '66.0.3359.181',
    captchaCheck: '0',
    osName: 'win',
    password: 'newstart1844',
    username: 'office@boedele.at'
}; 

//may be used as body for quickupdate request
const qu_dates = {
    from : '10-Jun-2018',
    to : '12-Jun-2018'
}
const qu_body = {
    'arrivaldays.fri': 'on',
    'arrivaldays.mon': 'on',
    'arrivaldays.sat': 'on',
    'arrivaldays.sun': 'on',
    'arrivaldays.thu': 'on',
    'arrivaldays.tue': 'on',
    'arrivaldays.wed': 'on',
    arrivaldaysAll: 'on',
    parentval: '',
    pricegroup1_chk: 'on',
    pricegroup1_txt: price_val.toString(),
    room: '145296:',
    'Komfort Suite': [ '', '' ],
    selectroom: '145296:',
    validityfrom: qu_dates.from,
    validityto: qu_dates.to 
};

login = () => {
    return new Promise((resolve, reject) => {

        var opt_Login = {
            "method": "POST",
            "hostname": "che.mappingmaster.com",
            "path": "/CultuzzHotelExtranet/index.htmf",
            "headers": {
                "content-type": "application/x-www-form-urlencoded",
                "origin": "https://che.mappingmaster.com",
                "referer": "https://che.mappingmaster.com/CultuzzHotelExtranet/index.htmf"
            }
        };

        var req_Login = http.request(opt_Login, function (res) {
            if(res.error){
                console.log('error:', res.error); // Print the error if one occurred
                reject(res.error);
                return res.error;
            }
            
            console.log('statusCode:', res && res.statusCode); // Print the response status code if a response was received

            var header = res.headers;

            cookie = header['set-cookie'];
            
            //_____________
            // var cookieString = cookie.join(";");

            // console.log("cookieString: " + cookieString);

            // var session_reg = /(JSESSIONID=[^;]+);/g;

            // var sessionID = session_reg.exec(cookieString)[1];
            // //console.log("sessionID: " + sessionID);

            // //var sessionID = cookieString.match(/JSESSIONID=[^;]+;/);

            // var host_reg = /(host=[^;]+);/g;

            // var host = host_reg.exec(cookieString)[1];
            // //console.log("host: " + host);

            // //var host = cookieString.match(/host=[^;]+;/);

            // console.log(sessionID + "\n" + host);

            // //JSESSIONID=0qdib5kif3u8cwRvxY0ZlxHR.undefined; host=84.115.217.171; _ga=GA1.2.64615041.1527754021; _gid=GA1.2.1009432612.1527754021
            // //console.log(cookie.join(";"));

            // var sendingCookie = sessionID + ' ' + host  + ' _ga=GA1.2.64615041.1527754021; _gid=GA1.2.1009432612.1527754021';
            //_____________
            //var sendingCookie = 'JSESSIONID=dE+kZzaKR+YZ3auEUX2LuD6D.undefined; host=84.115.217.171; _ga=GA1.2.64615041.1527754021; _gid=GA1.2.1009432612.1527754021';

            //JSESSIONID=E4e9bLgsrJWhhxXrX1DaqHc7.undefined
            //host=84.115.217.171

            cookie = postmanSession ? ['JSESSIONID=' + staticSessionID,'host=' + staticHostIP] : cookie; //[sessionID, host];

            var chunks = [];

            res.on("data", function (chunk) {
                chunks.push(chunk);
            });

            res.on("end", function () {
                var body = Buffer.concat(chunks);
                console.log(body.toString());
                // //try to extract a scriptSessionId from response and set new one
                // try{
                //     let reg = /dwr\.engine\.remote\.handleNewScriptSession\(\"(\d+|\w+)\"\)/;
                //     scriptSessionId = reg.exec(body)[1];
                //     lggr.log("captured scriptSessionId at login: " + scriptSessionId.blue);
                // }catch(e){
                        // if(!(e.message === "Cannot read property '1' of null")){
                        //     lggr.error(e);
                        // }
                //     lggr.log("could not capture a scriptSessionId at login");
                //     lggr.error(e);
                // }
            });

            var redirect_url = res.headers.location;

            //console.log("res.headers.location: " + redirect_url);

            redirect(redirect_url);

        });

        req_Login.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
            reject(new Error("Error on \'req_login\' - request"));
        });

        //"bName=chrome&bVersion=66.0.3359.181&captchaCheck=0&osName=win&password=newstart1844&username=office%40boedele.at"

        console.log('SENDING LOGIN REQUEST ...'.cyan);

        req_Login.write(
            qs.stringify(
                loginBodyObj
            )
        );
        req_Login.end();
        

        redirect = (uri) => {
            console.log("REDIRECTING to: ...\n".cyan + uri);
            
            // let cookieString = cookieArr[0] + "; " + cookieArr[1] + ";";
        
            // console.log("cookieString: " + cookieString);
        
            request({url: uri, headers: {Cookie: cookie}}, function (error, response, body) {
                if (error){
                    reject(error);
                    throw new Error(error);
                }
                console.log("REDIRECT RESPONSE --------".green);
                error ? console.log('error:', error) : null; // Print the error if one occurred
                console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                //console.log('body:',"\n" + body.split('\n').slice(1,3).join('\n') + "\n...\n"); // Print the HTML for the Google homepage.
        
                //console.log(JSON.stringify(response.headers, null ,2));

                console.log("\nCookie:");
                cookie.map(a => console.log(a.grey));
                console.log('\n');

                fs.writeFileSync("./loginResponseBody.txt", body);

                //try to extract a scriptSessionId from response and set new one
                try{
                    let reg = /dwr\.engine\.remote\.handleNewScriptSession\(\"(\d+|\w+)\"\)/;
                    scriptSessionId = reg.exec(body)[1];
                    lggr.log("captured scriptSessionId at login redirect response:\n" + scriptSessionId.blue);
                }catch(e){
                    if(!(e.message === "Cannot read property '1' of null")){
                        lggr.error(e);
                    }
                    lggr.log("could not capture a scriptSessionId at login redirect response");
                }
                
                resolve("LOGIN ACCOMPLISHED".magenta);
            });
        }
    });
}

// ________________________________________________________________________________________________________________________________________________________________________________

//not in use atm
postBody = (body) => {
    var opt_quickUpdate = {
        "method": "POST",
        "hostname": "che.mappingmaster.com",
        "path": "/CultuzzHotelExtranet/quickupdate.htm",
        "headers": {
          "Origin": "https://che.mappingmaster.com",
          "Upgrade-Insecure-Requests": "1",
          "Content-Type": "application/x-www-form-urlencoded",
          "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
          "X-DevTools-Emulate-Network-Conditions-Client-Id": "D388B134397C9BD85FFD7AFF46460765",
          "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
          "Referer": "https://che.mappingmaster.com/CultuzzHotelExtranet/quickupdate.htm?mn=5000",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language": "de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7",
          "Cache-Control": "no-cache",
          'connection' : 'keep-alive',
        }
    };

    var req = http.request(opt_quickUpdate, function (res) {
        console.log("-- QU callback -----------------------------------------------------------------------------------------------------------------------------------------");
        console.log('statusCode:', res && res.statusCode);      // Print the response status code if a response was received
    
        console.log(JSON.stringify(res.headers, null, 2));
    
      var chunks = [];
    
      res.on("data", function (chunk) {
        chunks.push(chunk);
      });
    
      res.on("end", function () {
        var body = Buffer.concat(chunks);
        console.log(body.toString());
      });
      
    });

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);  
    });

    // -------------------------------------------------------------- SET COOKIE -------------------------------------------------------------------
    req.setHeader("Cookie", cookie);
    console.log("Header Cookie: " + req.getHeader('Cookie').toString());

    console.log("NOW SENDING QU Request with Cookie: ".cyan + cookie);

    req.write(
        qs.stringify( body )
    );

    req.end();
}

updateRange = (days_obj) => {

    let cookieString = cookie.join(";");

    let session_reg = /(JSESSIONID=[^;]+);/g;

    let sessionID = session_reg.exec(cookieString)[1];

    var body_string = MMconv.toMMPost(days_obj, sessionID, scriptSessionId);

    return new Promise((resolve, reject) => {

        var options = {
            method: 'POST',
            url: 'https://che.mappingmaster.com/CultuzzHotelExtranet/dwr/call/plaincall/FourteenDayCalDWR.updateFourteenDayCalendar.dwr',
            gzip: true,
            headers: 
            { 
                'cache-control': 'no-cache',
                cookie: cookie,
                'accept-language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
                'accept-encoding': 'gzip, deflate, br',
                referer: 'https://che.mappingmaster.com/CultuzzHotelExtranet/fourteendaycal.htm?mn=5000',
                accept: '*/*',
                'content-type': 'text/plain',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
                origin: 'https://che.mappingmaster.com'
            },
            body: body_string
            
        };

        console.log("\n\n" + "SENDING Range Update Request ...".cyan);

        request(options, function (error, response, body) {
            if (error){
                reject();
                throw new Error(error);
            }
            console.log("FourteenDayCal RESPONSE --------".green);
            console.log('statusCode:', response && response.statusCode);
            console.log('body: ');
            console.log(body);
            resolve();
        });
    });
}

getZimmerRange = (query_obj) => {
    //respose data field LEGEND:
    // Avail - Verfügbarkeit
    // MinLS - Mindestaufenthaltsdauer (MinAD)
    // MaxLS - Maximale Aufenthaltsdauer (MaxAD)
    // CFS - Kein Verkauf möglich
    // CTB - Keine Buchungen möglich
    // CTA - Keine Anreise möglich
    // CTD - Keine Abreise möglich
    // BG - Buchungsgarantie
    
    // ORIGINAL REQUEST
    // callCount=1
    // windowName=
    // c0-scriptName=FourteenDayCalDWR
    // c0-methodName=getFourteenDayAvailability
    // c0-id=0
    // c0-e1=string:145296
    // c0-e2=string:145298
    // c0-e3=string:145300
    // c0-param0=array:[reference:c0-e1,reference:c0-e2,reference:c0-e3]
    // c0-param1=string:2018-06-06
    // c0-param2=string:2019-01-31
    // c0-param3=null:null
    // c0-param4=null:null
    // c0-param5=string:1
    // batchId=3
    // page=%2FCultuzzHotelExtranet%2Ffourteendaycal.htm%3Fmn%3D5000
    // httpSessionId=qd1AfvENHZ+0frUFti0Uttd0.undefined
    // scriptSessionId=83B939E38B02F586219D49121C6E1018

    //ALT STRING GENERATOR
    // let gZRBodyObj = {
    //     callCount: 1,
    //     windowName: '',
    //     'c0-scriptName': 'FourteenDayCalDWR',
    //     'c0-methodName': 'getFourteenDayAvailability',
    //     'c0-id': 0,
    //     'c0-e1': 'string: ' + rooms.cat1,
    //     'c0-e2': 'string: ' + rooms.cat2,
    //     'c0-e3': 'string: ' + rooms.cat3,
    //     'c0-param0': 'array:[reference:c0-e1,reference:c0-e2,reference:c0-e3]',
    //     'c0-param1': 'string:' + ZR_Range.begin,
    //     'c0-param2': 'string:' + ZR_Range.end,
    //     'c0-param3': 'null:null',
    //     'c0-param4': 'null:null',
    //     'c0-param5': 'string:1',
    //     batchId: 3,
    //     page:'%2FCultuzzHotelExtranet%2Ffourteendaycal.htm%3Fmn%3D5000',
    //     httpSessionId: sessionID,
    //     scriptSessionId:'83B939E38B02F586219D49121C6E1018',
    // };

    //let postBody = qs.stringify( gZRBodyObj );


    return new Promise( (resolve, reject) => {

        if (!query_obj) {
            reject("getZimmerRange: no quera_obj, cannot request");
            throw new Error("getZimmerRange: no query_obj, cannot request"); 
        }
        
        //extract JSESSIONID string for parameter sent in body
        try {
            let session_reg = /JSESSIONID=([^;]+);/g;

            var sessionID = session_reg.exec(cookie[0])[1];
        }catch(e) {
            var sessionID = 'JSESSIONID=2FmpwQVsULl0lDRNL15rikgL.undefined';
        }

        //use alternative hardcoded cookie or scriptSessionId - debugging
        //cookie= ['JSESSIONID=F3q4JUAl+bfTeNb1tZln+16k.undefined; Path=/CultuzzHotelExtranet; Secure', 'host=84.115.217.171', 'host=84.115.217.171'];

        //scriptSessionId = '83B939E38B02F586219D49121C6E1018';
        //scriptSessionId ='523109870987987A24351F19656A1E';

        let rawTxtBody = 
            `callCount=1
            windowName=
            c0-scriptName=FourteenDayCalDWR
            c0-methodName=getFourteenDayAvailability
            c0-id=0
            ${query_obj.room_cats.reduce((result, a, i) => result + (i===0 ? '' : '\n') + `c0-e${i+1}=string:`+ a.id,"")}
            c0-param0=array:[${query_obj.room_cats.map((a,i)=> 'reference:c0-e' + (i+1)).toString()}]
            c0-param1=string:${query_obj.range.from}
            c0-param2=string:${query_obj.range.to}
            c0-param3=null:null
            c0-param4=null:null
            c0-param5=string:1
            batchId=3
            page=%2FCultuzzHotelExtranet%2Ffourteendaycal.htm%3Fmn%3D5000
            httpSessionId=${sessionID}
            scriptSessionId=${scriptSessionId}`
            .dedent();


        var options = {
            method: 'POST',
            url: 'https://che.mappingmaster.com/CultuzzHotelExtranet/dwr/call/plaincall/FourteenDayCalDWR.getFourteenDayAvailability.dwr',
            gzip: true,
            headers: 
                {
                    'cache-control': 'no-cache',
                    cookie: cookie,
                    'accept-language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
                    'accept-encoding': 'gzip, deflate, br',
                    referer: 'https://che.mappingmaster.com/CultuzzHotelExtranet/fourteendaycal.htm?mn=5000',
                    accept: '*/*',
                    'content-type': 'application/x-www-form-urlencoded',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
                    'x-devtools-emulate-network-conditions-client-id': '3C0EAC10AB613E526CC53F124DD5AB8A',
                    origin: 'https://che.mappingmaster.com'
                },
            body: rawTxtBody
        };

        console.log("\n\n" + "SENDING ZimmerRange Request ...".cyan);
        console.log("sessionID: " + sessionID.yellow);
        console.log("reqZR cookie: ");
        cookie.map(a => console.log(a.grey));
        
        setTimeout(() => sendRequest(), 50);       //Set a delay if nessesary

        var sendRequest = () => {
            request(options, function (error, response, body) {
                if (error) throw new Error(error);

                lggr.log("reqZR RESPONSE --------".green);
                lggr.log('statusCode:', response && response.statusCode);

                //write plain response to file
                let filename = query_obj.saveResponse ? query_obj.saveResponse : "zimmer_range_response";
                fs.writeFileSync(filename + '.txt', beautify(body,{ indent_size: 2 }));

                //try to extract a scriptSessionId from response and set new one
                try{
                    let reg = /dwr\.engine\.remote\.handleNewScriptSession\(\"(\d+|\w+)\"\)/;
                    scriptSessionId = reg.exec(body)[1];
                    lggr.log("captured scriptSessionId: " + scriptSessionId.blue);
                }catch(e){
                    if(!(e.message === "Cannot read property '1' of null")){
                        lggr.error(e);
                    }
                    lggr.log("could not capture a new scriptSessionId");
                }
                
                //PARSE and VERYFY
                let ZRObj = MMResponseToObj(body);
                if (ZRObj instanceof Error) { lggr.log(ZRObj); reject(ZRObj); return ZRObj; }
                if (ZRObj[2] === null) { lggr.error("no price array in response obj"); fs.writeFileSync('./zimmer_range_response.json', ''); reject("no price array in response obj"); return ZRObj; }
                
                //write parsed Obj to JSON file
                fs.writeFileSync(filename + '.json', JSON.stringify(ZRObj, null, 2));

                //Output first n Lines of parsed Obj
                //let nLines = 10;
                //console.log('parsed response:',"\n" + JSON.stringify(ZRObj, null, 2).split('\n').slice(0,nLines).join('\n') + "\n...\n");

                resolve(ZRObj);
            });
        }
    });
}

// ________________________________________________________________________________________________________________________________________________________________________________

logout = () => {
    return new Promise( (resolve, reject) => {
        var options = {
            method: 'GET',
            url: 'https://che.mappingmaster.com/CultuzzHotelExtranet/successlogout.htmf',
            gzip: true,
            headers: {
                'Cache-Control': 'no-cache',
                Cookie: cookie,
                'Accept-Language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
                'Accept-Encoding': 'gzip, deflate, br',
                Referer: 'https://che.mappingmaster.com/CultuzzHotelExtranet/mainmasterdata.htm?mn=3001',
                Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'X-DevTools-Emulate-Network-Conditions-Client-Id': 'E1D788862E18B22DC2A0C0FCF5534AC5',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
                'Upgrade-Insecure-Requests': '1' 
            }
        };

        console.log("LOGGING OUT with COOKIE: ...".cyan);
        cookie.map((a) => {console.log(a.grey)});

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            //console.log(body);
            resolve("Client sucessfully logged out".magenta);
        });
    });
}

module.exports = {
    login: login,
    request: postBody,
    logout: logout,
    getZimmerRange: getZimmerRange,
    updateRange: updateRange
}